import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const h2Element = getByText(/this is our library component/i);
  expect(h2Element).toBeDefined();
});
