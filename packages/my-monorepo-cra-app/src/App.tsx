import React from 'react';
import logo from './logo.svg';
import './App.css';
import { MyNewComponent } from '@my-org/my-monorepo-ui-lib';
// eslint-disable-next-line
import { EchoService } from '@my-org/api-client';

function App(): JSX.Element {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <MyNewComponent text="this is our library component" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
