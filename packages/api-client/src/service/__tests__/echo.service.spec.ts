import { EchoService } from '../echo.service';
import { BASE_URL } from '../echo.client';
import { MessageRequest } from '@my-org/api-interface';
import nock from 'nock';

test('works with nock and  async/await', async () => {
  const message: MessageRequest = {
    message: 'Hello World',
  };
  nock(BASE_URL)
    .post('/post')
    .reply(200, {
      data: message,
    });

  const response = await EchoService.postEcho(message);
  expect(response).toEqual(message);
});
