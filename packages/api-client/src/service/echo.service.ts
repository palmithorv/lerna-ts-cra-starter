import { MessageRequest, MessageResponse } from '@my-org/api-interface';
import { EchoClient } from './echo.client';

const postEcho = async (message: MessageRequest): Promise<MessageResponse> => {
  const response = await EchoClient.post('/post', message);
  return response.data.data;
};

export const EchoService = {
  postEcho,
};
