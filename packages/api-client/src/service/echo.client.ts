import axios from 'axios';

export const BASE_URL = 'https://postman-echo.com';

export const EchoClient = axios.create({
  baseURL: BASE_URL,
});
