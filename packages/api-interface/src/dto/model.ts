export interface MessageRequest {
  message: string;
}

export interface MessageResponse {
  message: string;
}

export interface UpdatedModel {
  message: string;
}
