import { MessageRequest } from './model';

test('example', () => {
  const message: MessageRequest = {
    message: 'hello world',
  };
  expect(message.message).toEqual('hello world');
});
